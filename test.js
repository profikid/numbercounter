import test from 'ava';
import GuessNumberGame from './model/GuessNumberGame';

test.beforeEach(t => {
	t.context.game = new GuessNumberGame(1,50);
});


test('check if predefined number is set', t=>{
	
	t.false(isNaN(t.context.game.guessNumber));
});

test('check if number is between range',t=>{
	var game = t.context.game;
	var min = game.minRange;
	var max = game.maxRange;
	t.true(game.getRandomNumber() >= min);
	t.true(game.getRandomNumber() <= max);

});

test('should throw error if attempt more than expected', t=>{
	t.context.game.guess(1);
	t.context.game.guess(1);
	t.context.game.guess(1);
	t.throws(()=> t.context.game.guess(1));
});

test('should return true when guess is matched', t=>{
	var cheatyGuess = t.context.game.guessNumber;
	t.true(t.context.game.guess(cheatyGuess));
});




