'use strict';

module.exports = class GuessNumberGame{
	constructor(min, max){
		this.minRange = min;
		this.maxRange = max;
		this.maxAttempts = 3;
		this.currentAttempt = 0;
		this.guessNumber = this.getRandomNumber();
	}

	getRandomNumber(){
		return Math.floor(Math.random() * (this.maxRange-this.minRange) + this.minRange);
	}

	guess(attempt){
		if(!this.enabled())
			throw new Error('GuessNumberGame is not enabled');

		this.currentAttempt++;
		return (this.guessNumber == attempt);
	}

	enabled(){
		return this.currentAttempt < this.maxAttempts;
	}
}