'use strict';
const prompt = require('prompt');
const low = require('lowdb');
const storage = require('lowdb/file-sync');
const GuessNumberGame = require('./GuessNumberGame');
const db = low('db.json', { storage });
const colors = require('colors');

//config range
const min = 1;
const max = 50;


module.exports = class GameManager{
	constructor(){
		this.setup();
	}

	setup(){
		this.game = new GuessNumberGame(min,max);

		this.questionMessage = {
			properties:{
				guess:{
					required: true,
					type: 'integer',
					message:"only numbers are accepted",
					description:`Try a guess min ${this.game.minRange} and max ${this.game.maxRange}`
				}
			}
		};
	}


	start(){
		prompt.start();
		console.log(`You will have ${this.game.maxAttempts} attempts to guess a number`.bold);
		this.question();
	}

	question(){
		let totalAttempts = db.object.guesses.length
		console.log("\nCurrent Attempt: ".bold, this.game.currentAttempt + 1 );
		console.log("Total Attempts: ".bold, totalAttempts +"\n");

		prompt.get(this.questionMessage, this.parseAnswer.bind(this));
	}

	parseAnswer(err, result){
		let answer = result.guess;
		db('guesses').push(answer);

		if(this.game.guess(answer)){
			console.log("YOU WON".underline.green);
			return;
		}
		console.log("WRONG".underline.red);
		this.reAttempt();
	}

	reAttempt(){
		if(this.game.enabled()){
			this.question();
			return;
		}
		console.log("\nThe guessing number was: ".bold, this.game.guessNumber);
		console.log("game over".underline.red);
	}

};